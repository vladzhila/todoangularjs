(function() {
	"use strict";

	angular
		.module("todoMVC")
		.config(["localStorageServiceProvider",	function(localStorageServiceProvider) {
			localStorageServiceProvider
				.setPrefix('ls');
		}]);

})();