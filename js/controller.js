(function() {
	"use strict";

	angular
		.module("todoMVC")
		.controller("TodoCtrl", TodoService);

	TodoService.$inject = ["$scope", "localStorageService"];

	function TodoService($scope, localStorageService) {

		var vm = this;

		vm.hidden = false;

		vm.init = function() {
			vm.list = JSON.parse(localStorageService.get("todoLs") || "[]");
		};

		$scope.$watch("vm.list", function(newVal, oldVal) {

			if (newVal !== null && angular.isDefined(newVal) && newVal !== oldVal) {
				localStorageService.add("todoLs", angular.toJson(newVal));
			}

		}, true);

		vm.add = function() {

			if (vm.newItem.length != 0) {

				vm.list.push({
					item: vm.newItem,
					done: false,
					nested: false,
					open: true,
					line: true,
					nodes: []
				});

				vm.newItem = "";
			}

		};

		vm.addNode = function(todo) {

			todo.nodes.push({
				item: "",
				done: false,
				nested: true,
				open: true,
				line: true,
				nodes: []
			});

		};

		vm.toggleOpenClose = function(todo) {
			todo.open ? todo.open = false : todo.open = true;
		};

		vm.remove = function(todo) {
			var idx = vm.list.indexOf(todo);
			(~idx) ? vm.list.splice(idx, 1) : todo.nodes = [];
		};

	} 

})();
